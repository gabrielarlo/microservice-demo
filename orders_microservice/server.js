const express = require('express');
const fetch = require('node-fetch');
const mongoose = require('mongoose');
const Orders = require('./orderSchema');

const app = express();
const router = express.Router();

const port = 3002;

const userBaseURL = 'http://127.0.0.1:3001';

router.use((req, res, next) => {
    console.log('/' + req.method);
    console.log(req.body);
    next();
});
app.use(express.json());

const mongoDB = 'mongodb://localhost:27017/OrderService';
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: true});

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error: '));

db.once('open', () => {
    console.log('MongoDB database connection established!');
});


// get status
router.get('/status', (req, res) => {
    res.send('Order Service is running!');
});

// get orders
router.get('/records', (req, res) => {
    Orders.find({}, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

// get orders by user id
router.get('/records-by-user-id', (req, res) => {
    const id = req.query.id;
    if (id == undefined) {
        res.send({error: 'Undefined id'});
        return;
    }
    Orders.find({userID: id}, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

// get order details
router.get('/get-details', async (req, res) => {
    const id = req.query.id;
    if (id == undefined) {
        res.send({error: 'Undefined id'});
        return;
    }
    try {
        const result = await Orders.findById(id, { __v: 0 });
        const userID = result.userID;
        const fetched = await fetch(userBaseURL + '/get-details?id=' + userID);
        const data = await fetched.json();
        
        const ret = {
            order: result,
            user: data,
        };
        res.send(ret);
    } catch (error) {
        res.send(error);
    }
});

// add order
router.post('/insert', (req, res) => {
    const data = req.body;
    Orders.insertMany(data, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

// update order status
router.put('/update-status', async (req, res) => {
    const id = req.body.id;
    if (id == undefined) {
        res.send({error: 'Undefined id'});
        return;
    }
    try {
        const result = await Orders.findById(id);
        const status = result.status;

        Orders.updateOne({_id: id}, {
            $set: { status: status == 1 ? 0 : 1, updatedAt: Date.now() }
        }, (err, result) => {
            if (err) {
                res.send(err);
            } else {
                res.send(result);
            }
        });
    } catch (error) {
        res.send(error);
    }
});

// delete related order by user
router.delete('/delete-by-user-id', async (req, res) => {
    const userID = req.body.userID;
    if (userID == undefined) {
        res.send({error: 'Undefined userID'});
        return;
    }
    try {
        const result = await Orders.deleteMany({userID: userID});
        res.send(result);
    } catch (error) {
        res.send(error);
    }
});

app.use('/', router);

app.listen(port, () => {
    console.log('Order Service is running on port ' + port);
});
