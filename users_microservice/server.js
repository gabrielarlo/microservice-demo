const express = require('express');
const fetch = require('node-fetch');
const mongoose = require('mongoose');
const Users = require('./userSchema');

const app = express();
const router = express.Router();

const port = 3001;

const orderBaseURL = 'http://127.0.0.1:3002';

router.use((req, res, next) => {
    console.log('/' + req.method);
    console.log(req.body);
    next();
});
app.use(express.json());


// MongoDB init
// const mongoDB = 'mongodb+srv://dbGabriel:dbPassword123@userservice.i23yd.mongodb.net/UserService?retryWrites=true&w=majority';
const mongoDB = 'mongodb://localhost:27017/UsersService'
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: true});

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error: '));

db.once('open', () => {
    console.log('MongoDB database connection established!');
});

// get status
router.get('/status', (req, res) => {
    res.send('User Service is running');
});

// get records
router.get('/records', (req, res) => {
    Users.find({}, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

// get basic details
router.get('/get-details', (req, res) => {
    const id = req.query.id;
    if (id == undefined) {
        res.send({error: 'Undefined id'});
        return;
    }
    Users.findById(id, {name: 1, age: 1}, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

// add user
router.post('/insert', (req, res) => {
    const data = req.body;
    Users.insertMany(data, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

// update user
// can use post
router.put('/update', (req, res) => {
    const id = req.body.id;
    if (id == undefined) {
        res.send({error: 'Undefined id'});
        return;
    }
    const data = req.body;
    Users.updateOne({_id: id}, {
        $set: { name: data.name, age: data.age, updatedAt: Date.now() }
    }, (err, result) => {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
});

// delete user
router.delete('/delete', async (req, res) => {
    const id = req.body.id;
    if (id == undefined) {
        res.send({error: 'Undefined id'});
        return;
    }
    try {
        await Users.deleteOne({_id: id});
        
        const fetched = await fetch(orderBaseURL + '/delete-by-user-id', {
            method: 'DELETE',
            body: JSON.stringify({userID: id}),
            headers: { 'Content-Type': 'application/json' },
        });
        
        const data = await fetched.json();
        res.send(data);
    } catch (error) {
        res.send(error);
    }
});


app.use('/', router);

app.listen(port, () => {
    console.log('User Service is running on port ' + port);
});