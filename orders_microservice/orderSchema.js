const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    orderNumber: {
        type: String,
        required: [true, 'Order Number is required'],
    },
    userID: {
        type: String,
        required: [true, 'User ID is required'],
    },
    totalQty: {
        type: Number,
        required: [true, 'Age is required'],
    },
    totalAmount: {
        type: Number,
        required: [true, 'Age is required'],
    },
    status: {
        type: Number,
        default: 1,
    },
    updatedAt: {
        type: Date,
        default: Date.now,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
}, {
    collection: 'Orders'
});

module.exports = mongoose.model('Orders', orderSchema);