const express = require('express');
const fetch = require('node-fetch');

const app = express();
const router = express.Router();

const port = 8080;

const userBaseUrl = 'http://127.0.0.1:3001';
const orderBaseUrl = 'http://127.0.0.1:3002';

router.use((req, res, next) => {
    console.log('/' + req.method);
    console.log(req.body);
    const authorization = req.headers.authorization;
    console.log(authorization);
    if (authorization == undefined) {
        res.send('Invalid Token');
        return;
    }
    next();
});
app.use(express.json());

router.get('/status', (req, res) => {
    res.send('Browser API is running');
});

// get all users
router.get('/get-all-users', async (req, res) => {
    try {
        const fetched = await fetch(userBaseUrl + '/records');
        const data = await fetched.json();
        res.send(data);
    } catch (error) {
        res.send(error);
    }
});


// get all orders
router.get('/get-all-orders', async (req, res) => {
    try {
        const fetched = await fetch(orderBaseUrl + '/records');
        const data = await fetched.json();
        res.send(data);
    } catch (error) {
        res.send(error);
    }
});

// get user details and orders
router.get('/get-user-details', async (req, res) => {
    const id = req.query.id;
    if (id == undefined) {
        res.send({error: 'Undefined id'});
        return;
    }
    try {
        const fetchedUser = await fetch(userBaseUrl + '/get-details?id=' + id);
        const userJson = await fetchedUser.json();
        const fetchedOrder = await fetch(orderBaseUrl + '/records-by-user-id?id=' + id);
        const orderJson = await fetchedOrder.json();
        res.send({
            user: userJson,
            orders: orderJson,
        });
    } catch (error) {
        res.send(error);
    }
});


// get order details and user
router.get('/get-order-details', async (req, res) => {
    const id = req.query.id;
    if (id == undefined) {
        res.send({error: 'Undefined id'});
        return;
    }
    try {
        const fetched = await fetch(orderBaseUrl + '/get-details?id=' + id);
        const data = await fetched.json();
        res.send(data);
    } catch (error) {
        res.send(error);
    }
});


// add new user
router.post('/add-new-user', async (req, res) => {
    try {
        const data = req.body;
        const fetched = await fetch(userBaseUrl + '/insert', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        });
        const json = await fetched.json();
        res.send(json);
    } catch (error) {
        res.send(error);
    }
});

// add new order
router.post('/add-new-order', async (req, res) => {
    try {
        const data = req.body;
        const fetched = await fetch(orderBaseUrl + '/insert', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        });
        const json = await fetched.json();
        res.send(json);
    } catch (error) {
        res.send(error);
    }
});

// update order status
router.post('/update-order-status', async (req, res) => {
    try {
        const data = req.body;
        const fetched = await fetch(orderBaseUrl + '/update-status', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        });
        const json = await fetched.json();
        res.send(json);
    } catch (error) {
        res.send(error);
    }
});


// update user
router.post('/update-user', async (req, res) => {
    try {
        const data = req.body;
        const fetched = await fetch(userBaseUrl + '/update', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        });
        const json = await fetched.json();
        res.send(json);
    } catch (error) {
        res.send(error);
    }
});


// delete user
router.delete('/delete-user', async (req, res) => {
    try {
        const data = req.body;
        const fetched = await fetch(userBaseUrl + '/delete', {
            method: 'DELETE',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        });
        const json = await fetched.json();
        res.send(json);
    } catch (error) {
        res.send(error);
    }
});

app.use('/', router);

app.listen(port, () => {
    console.log('Browser API is running on port ' + port);
});